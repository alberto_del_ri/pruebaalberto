-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.7.12-log - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura de base de datos para bd_productos
CREATE DATABASE IF NOT EXISTS `bd_productos` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `bd_productos`;


-- Volcando estructura para tabla bd_productos.categorias_00
CREATE TABLE IF NOT EXISTS `categorias_00` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(160) DEFAULT NULL,
  `description` varchar(258) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;



-- Volcando estructura para tabla bd_productos.productos_00
CREATE TABLE IF NOT EXISTS `productos_00` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(160) DEFAULT NULL,
  `description` varchar(258) DEFAULT NULL,
  `image` longblob,
  `id_categoria` int(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_categoria` (`id_categoria`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla bd_productos.productos_00: 2 rows
/*!40000 ALTER TABLE `productos_00` DISABLE KEYS */;
/*!40000 ALTER TABLE `productos_00` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
