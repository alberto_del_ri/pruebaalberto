package com.example.prueba.repositories;

import com.example.prueba.models.Categoria;
import org.springframework.data.repository.CrudRepository;

/**
 * Repositorio de productos.
 * 
 * @author mtellechea
 */
public interface CategoriaRepository extends CrudRepository<Categoria, Integer> {

	public Categoria getCategoriaByNombre(String nombre);
}