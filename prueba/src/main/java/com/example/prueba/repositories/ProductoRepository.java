package com.example.prueba.repositories;

import com.example.prueba.models.Producto;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

/**
 * Repositorio de productos.
 * 
 * @author obarcia
 */
public interface ProductoRepository extends CrudRepository<Producto, Integer> {
	
	public List<Producto> getProductoByCategoriaId(Integer id);
}