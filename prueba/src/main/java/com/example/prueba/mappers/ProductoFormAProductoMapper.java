package com.example.prueba.mappers;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import com.example.prueba.models.Categoria;
import com.example.prueba.models.Producto;
import com.example.prueba.models.ProductoForm;

public class ProductoFormAProductoMapper {

	public static Producto productoFormAProducto (ProductoForm productoForm, Producto producto, Categoria categoria) {
		producto.setDescripcion(productoForm.getDescripcion());
		producto.setNombre(productoForm.getNombre());
		producto.setCategoria(categoria);
		return producto;	
	}
	
	public static ProductoForm productoAProductoForm (Producto producto) {
	ProductoForm productoForm = new ProductoForm();
	productoForm.setCategoria(producto.getCategoria().getNombre());
	productoForm.setDescripcion(producto.getDescripcion());
	productoForm.setNombre(producto.getNombre());
	
	return productoForm;
	}

}
