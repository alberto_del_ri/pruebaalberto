package com.example.prueba.services;

import com.example.prueba.exceptions.CategoriaNotFoundException;
import com.example.prueba.exceptions.ProductoNotFoundException;
import com.example.prueba.models.Categoria;
import java.util.List;

/**
 * Interface del servicio de productos.
 * 
 * @author obarcia
 */
public interface CategoriaService
{
    /**
     * Devuelve el listado de Categorias.
     * @return Listado.
     */
    public List<Categoria> getAll();
    /**
     * Devuelve una categoria por su identificador.
     * @param id Identificador de la categoria
     * @return Instancia del producto.
     * @throws ProductoNotFoundException 
     */
    public Categoria getOne(Integer id) throws CategoriaNotFoundException;
    /**
     * Guardar la categoria.
     * @param producto Instancia del producto.
     * @return Producto guardado.
     * @throws java.lang.Throwable
     */
    public Categoria save(Categoria categoria) throws Throwable;
    /**
     * Eliminar una categoria.
     * @param producto Instancia de la categoria.
     * @throws Throwable 
     */
    public void delete(Categoria categoria) throws Throwable;
    
    /**
     * Busca una categoria por nombre. 
     * @param categoria
     * @return
     */
    public Categoria getCategoriaByNombre(String nombre); 
}