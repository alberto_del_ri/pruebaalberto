package com.example.prueba.services;

import com.example.prueba.exceptions.ProductoNotFoundException;
import com.example.prueba.mappers.ProductoFormAProductoMapper;
import com.example.prueba.models.Producto;
import com.example.prueba.models.ProductoForm;
import com.example.prueba.repositories.ProductoRepository;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementación del servicio de productos.
 * 
 * @author obarcia
 */
@Service
public class ProductoServiceImpl implements ProductoService
{
    /**
     * Repositorio de productos.
     */
    @Autowired
    private ProductoRepository productoRepository;
    
    @Override
    public List<Producto> getAll()
    {
        List<Producto> data = new ArrayList<>();
        data =  (List<Producto>) productoRepository.findAll();
        return data;
    }
    @Override
    public Producto getOne(Integer id) throws ProductoNotFoundException
    {
        Optional<Producto> oproducto = productoRepository.findById(id);
        if (oproducto.isPresent()) {
            Optional<Producto> data =  productoRepository.findById(id);
            return data.get();
        }
        
        throw new ProductoNotFoundException();
    }
    @Override
    @Transactional(rollbackFor=Exception.class)
    public Producto save(Producto producto) throws Throwable
    {
        return productoRepository.save(producto);
    }
    @Override
    @Transactional(rollbackFor=Exception.class)
    public void delete(Producto producto) throws Throwable
    {
        productoRepository.delete(producto);
    }
    @Override
    @Transactional(readOnly=true)
    public InputStream getImage(Integer id) throws Throwable
    {
        Producto producto = getOne(id);
        
        byte[] data = producto.getImagen();
        
        if (data != null) {
            return new ByteArrayInputStream(data);
        } else {
            return new ByteArrayInputStream(new byte[]{});
        }
    }
    @Override
    public void setImage(Producto producto, InputStream stream) throws Throwable
    {
        byte fileContent[] = new byte[(int)stream.available()];

        stream.read(fileContent);
        
        producto.setImagen(fileContent);
    }
	@Override
	public List<ProductoForm> getAllDTO() {
		// TODO Auto-generated method stub
		return null;
	}
	
	 public List<ProductoForm> getProductoByCategoriaId(Integer id)
	 {
	    	return productoRepository.getProductoByCategoriaId(id).stream().map(p-> ProductoFormAProductoMapper.productoAProductoForm(p)).collect(Collectors.toList());

	 }
}