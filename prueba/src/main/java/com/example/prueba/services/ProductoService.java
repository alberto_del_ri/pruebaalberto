package com.example.prueba.services;

import com.example.prueba.exceptions.ProductoNotFoundException;
import com.example.prueba.models.Producto;
import com.example.prueba.models.ProductoForm;

import java.io.InputStream;
import java.util.List;

/**
 * Interface del servicio de productos.
 * 
 * @author obarcia
 */
public interface ProductoService
{
    /**
     * Devuelve el listado de productos.
     * @return Listado.
     */
    public List<Producto> getAll();
    
    /**
     * Devuelve el listado de productos.
     * @return Listado.
     */
    public List<ProductoForm> getAllDTO();
    
    /**
     * Devuelve un producto por su identificador.
     * @param id Identificador del producto.
     * @return Instancia del producto.
     * @throws ProductoNotFoundException 
     */
    public Producto getOne(Integer id) throws ProductoNotFoundException;
    /**
     * Guardar el producto.
     * @param producto Instancia del producto.
     * @return Producto guardado.
     * @throws java.lang.Throwable
     */
    public Producto save(Producto producto) throws Throwable;
    /**
     * Eliminar un producto.
     * @param producto Instancia del producto.
     * @throws Throwable 
     */
    public void delete(Producto producto) throws Throwable;
    /**
     * Devuelve el InputStream de la imagen.
     * @param id Identificador del producto.
     * @return Instancia del InputStream.
     * @throws Throwable 
     */
    public InputStream getImage(Integer id) throws Throwable;
    /**
     * Asignar una imagen a un producto.
     * @param producto Instancia del producto.
     * @param stream Instancia del InputStream.
     * @throws Throwable 
     */
    public void setImage(Producto producto, InputStream stream) throws Throwable;
    
    public List<ProductoForm> getProductoByCategoriaId(Integer id);
}