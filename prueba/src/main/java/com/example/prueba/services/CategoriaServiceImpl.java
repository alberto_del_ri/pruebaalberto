package com.example.prueba.services;

import com.example.prueba.exceptions.CategoriaNotFoundException;
import com.example.prueba.models.Categoria;
import com.example.prueba.repositories.CategoriaRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementación del servicio de las categorias.
 * 
 * @author mtellechea
 */
@Service
public class CategoriaServiceImpl implements CategoriaService
{
    /**
     * Repositorio de Categorias.
     */
    @Autowired
    private CategoriaRepository categoriaRepository;
    
    @Override
    public List<Categoria> getAll()
    {
        List<Categoria> list = new ArrayList<>();
        categoriaRepository.findAll().iterator().forEachRemaining(list::add);
        return list;
    }
    
    @Override
    public Categoria getOne(Integer id) throws CategoriaNotFoundException
    {
        Optional<Categoria> ocategoria = categoriaRepository.findById(id);
        if (ocategoria.isPresent()) {
            return ocategoria.get();
        }
        
        throw new CategoriaNotFoundException();
    }
    @Override
    @Transactional(rollbackFor=Exception.class)
    public Categoria save(Categoria categoria) throws Throwable
    {
        return categoriaRepository.save(categoria);
    }
    @Override
    @Transactional(rollbackFor=Exception.class)
    public void delete(Categoria categoria) throws Throwable
    {
    	categoriaRepository.delete(categoria);
    }
   
    /**
     * Busca una categoria por nombre. 
     * @param categoria
     * @return
     */
    @Override
    @Transactional(rollbackFor=Exception.class)
    public Categoria getCategoriaByNombre(String nombre) 
    {
    	return categoriaRepository.getCategoriaByNombre(nombre);
    }
    
}