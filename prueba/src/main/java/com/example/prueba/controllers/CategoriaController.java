package com.example.prueba.controllers;

import com.example.prueba.models.Categoria;
import com.example.prueba.models.CategoriaForm;
import com.example.prueba.models.Producto;
import com.example.prueba.models.ProductoForm;
import com.example.prueba.services.CategoriaService;
import com.example.prueba.services.ProductoService;
import java.io.InputStream;
import java.util.Locale;
import javax.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Controlador web.
 * 
 * @author mtellechea
 */
@Controller
@RequestMapping("/categoria")
public class CategoriaController
{
    /**
     * Servicio de categorias.
     */
    @Autowired
    private CategoriaService categoriaService;
    
    /**
     * Página principal categorias.
     * @return Vista resultante.
     */
    @GetMapping
    public ModelAndView index()
    {
        return new ModelAndView("listaCategorias")
            .addObject("list", categoriaService.getAll());
    }
    /**
     * Página de un producto.
     * @param id Identificador del producto.
     * @return Vista resultante.
     * @throws java.lang.Throwable
     */
    @GetMapping("/{id}")
    public ModelAndView categoria(@PathVariable("id") Integer id) throws Throwable
    {
        Categoria categoria;
        if (id != 0) {
            categoria = categoriaService.getOne(id);
        } else {
            categoria = new Categoria();
        }
        
        // Actualizar el formulario
        CategoriaForm form = new CategoriaForm();
        BeanUtils.copyProperties(categoria, form);
        
        return new ModelAndView("categoria")
            .addObject("form", form);
    }
    /**
     * Actualizar / guardar un producto.
     * @param id Identificador del producto.
     * @param form Instancia del formulario.
     * @param file Fichero a subir.
     * @param result Instancia del resultado de validación.
     * @param locale Internacionalización.
     * @param flash Flash de respuesta.
     * @return Vista resultante.
     * @throws Throwable 
     */
    @PostMapping("/{id}")
    public ModelAndView categoriaPost(
            @PathVariable("id") Integer id,
            @Valid @ModelAttribute("form") ProductoForm form,
            @RequestParam(value="file", required=false) MultipartFile file,
            BindingResult result,
            Locale locale,
            RedirectAttributes flash) throws Throwable
    {
        Categoria categoria;
        if (id != 0) {
            categoria = categoriaService.getOne(id);
        } else {
            categoria = new Categoria();
        }
        
        
        
        // Si no hay errores
        if (!result.hasErrors()) {
            // Actualizar el producto
            BeanUtils.copyProperties(form, categoria);
            
            // Guardar la categoria
            categoria = categoriaService.save(categoria);

            // Añadir mensaje flash (I18N)
            flash.addFlashAttribute("flash", "Categoria guardada con éxito");

            // Redirect
            return new ModelAndView("redirect:/categoria");
        }

        // Si hubo errores se muestra el formulario
        return new ModelAndView("categoria")
            .addObject("form", form);
    }
    
    /**
     * Eliminar un producto.
     * @param id Identificador del producto.
     * @return Vista resultante.
     * @throws java.lang.Throwable
     */
    @GetMapping("/{id}/delete")
    public ModelAndView categoriaDelete(@PathVariable("id") Integer id) throws Throwable
    {
        Categoria categoria = categoriaService.getOne(id);
  
        categoriaService.delete(categoria);
        
        return new ModelAndView("redirect:/categoria");
    }
    
    /**
     * Eliminar un producto.
     * @param id Identificador del producto.
     * @return Vista resultante.
     * @throws java.lang.Throwable
     */
    @GetMapping("/{id}/borrarImagen")
    public ModelAndView b(@PathVariable("id") Integer id) throws Throwable
    {
        Categoria categoria = categoriaService.getOne(id);
  
        categoriaService.delete(categoria);
        
        return new ModelAndView("redirect:/categoria");
    }
}