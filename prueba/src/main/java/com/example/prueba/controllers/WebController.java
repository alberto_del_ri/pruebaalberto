package com.example.prueba.controllers;

import com.example.prueba.mappers.ProductoFormAProductoMapper;
import com.example.prueba.models.Categoria;
import com.example.prueba.models.Producto;
import com.example.prueba.models.ProductoForm;
import com.example.prueba.services.CategoriaService;
import com.example.prueba.services.ProductoService;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Controlador web.
 * 
 * @author obarcia
 */
@Controller
@RequestMapping("/")
public class WebController {
	/**
	 * Servicio de productos.
	 */
	@Autowired
	private ProductoService productoService;

	@Autowired
	private CategoriaService categoriaService;

	/**
	 * Página principal.
	 * 
	 * @return Vista resultante.
	 */
	@GetMapping
	public ModelAndView index() {
		return new ModelAndView("index").addObject("list", productoService.getAll());
	}

	/**
	 * Página de un producto.
	 * 
	 * @param id
	 *            Identificador del producto.
	 * @return Vista resultante.
	 * @throws java.lang.Throwable
	 */
	@GetMapping("/producto/{id}")
	public ModelAndView producto(@PathVariable("id") Integer id) throws Throwable {
		Producto producto;
		ProductoForm form = null;
		if (id != 0) {
			producto = productoService.getOne(id);
			form = ProductoFormAProductoMapper.productoAProductoForm(producto);
		} else {
			form = new ProductoForm();
		}

		return new ModelAndView("producto").addObject("form", form).addObject("categorias", categoriaService.getAll());
	}

	/**
	 * Actualizar / guardar un producto.
	 * 
	 * @param id
	 *            Identificador del producto.
	 * @param form
	 *            Instancia del formulario.
	 * @param file
	 *            Fichero a subir.
	 * @param result
	 *            Instancia del resultado de validación.
	 * @param locale
	 *            Internacionalización.
	 * @param flash
	 *            Flash de respuesta.
	 * @return Vista resultante.
	 * @throws Throwable
	 */
	@PostMapping("/producto/{id}")
	public ModelAndView productoPost(@PathVariable("id") Integer id, @Valid @ModelAttribute("form") ProductoForm form,
			@RequestParam(value = "file", required = false) MultipartFile file, BindingResult result, Locale locale,
			RedirectAttributes flash) throws Throwable {
		Producto producto;
		if (id != 0) {
			producto = productoService.getOne(id);
		} else {
			producto = new Producto();
		}

		;

		Categoria categoria = categoriaService.getCategoriaByNombre(form.getCategoria());
		// Si no hay errores
		if (!result.hasErrors() && validarFormYSettearImagen(result, flash, producto, file, form)) {
			// Actualizar el producto
			ProductoFormAProductoMapper.productoFormAProducto(form, producto, categoria);

			// Guardar el producto
			producto = productoService.save(producto);

			// Añadir mensaje flash (I18N)
			flash.addFlashAttribute("flash", "Producto guardado con éxito");

			// Redirect
			return new ModelAndView("redirect:/");
		}

		// Si hubo errores se muestra el formulario
		return new ModelAndView("producto").addObject("form", form).addObject("flash", flash).addObject("categorias", categoriaService.getAll());
	}

	private boolean validarFormYSettearImagen(BindingResult result, RedirectAttributes flash, Producto producto,
			MultipartFile file, ProductoForm form) throws IOException, Throwable {
		// Guardar el fichero
		boolean validarForm = true;
		
		if (form.getNombre()== null || form.getNombre().equals("")) {
			flash.addAttribute("Validacion Nombre:", "El nombre es obligatario");
			validarForm = false;
		}
		
		if (form.getNombre().length() > 160) {
			flash.addAttribute("Validacion:", "El nombre como maximo debe tener 160 caracteres.");
			validarForm = false;
		}
		if (form.getDescripcion().length() > 258) {
			flash.addAttribute("Validacion Descripcion:", "La descripcion  como maximo debe tener 258 caracteres.");
			validarForm = false;
		}
		
		if (form.getDescripcion()== null || form.getDescripcion().equals("")) {
			flash.addAttribute("Validacion:", "La descripcion es obligatoria");
			validarForm = false;
		}
		
		
		if (file.getContentType() != null && file.getSize()>0) {
			if (file.getContentType().equals("image/png") || (file.getContentType().equals("image/jpeg"))) {
				if (file.getSize() <= 1000000) {
					productoService.setImage(producto, file.getInputStream());
				} else {
					flash.addAttribute("Validacion Archivo", "El archivo no cumple con limite de tamano de 1 MB.");
					validarForm = false;
				}
			} else {
				flash.addAttribute("Validacion:", "El archivo no cumple con la extension requerida.");
				validarForm = false;
			}
		}
		return validarForm;
	}

	/**
	 * Devuelve la imagen de un producto.
	 * 
	 * @param id
	 *            Identificador del producto.
	 * @return Vista resultante.
	 * @throws java.lang.Throwable
	 */
	@GetMapping("/producto/{id}/image")
	public @ResponseBody ResponseEntity<Resource> productoImage(@PathVariable("id") Integer id) throws Throwable {
		InputStream is = productoService.getImage(id);
		InputStreamResource resource = new InputStreamResource(is);

		// Content disposition
		ContentDisposition contentDisposition = ContentDisposition.builder("inline").filename("image").build();

		// Headers
		HttpHeaders headers = new HttpHeaders();
		headers.setContentDisposition(contentDisposition);
		headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
		headers.add("Pragma", "no-cache");
		headers.add("Expires", "0");

		// Respuesta
		return ResponseEntity.ok().headers(headers).contentLength(is.available()).body(resource);
	}

	/**
	 * Eliminar un producto.
	 * 
	 * @param id
	 *            Identificador del producto.
	 * @return Vista resultante.
	 * @throws java.lang.Throwable
	 */
	@GetMapping("/producto/{id}/delete")
	public ModelAndView productoDelete(@PathVariable("id") Integer id) throws Throwable {
		Producto producto = productoService.getOne(id);

		productoService.delete(producto);

		return new ModelAndView("redirect:/");
	}

	@GetMapping("/producto/{id}/exportar")
	@ResponseBody
	public ProductoForm productoExportar(@PathVariable("id") Integer id) throws Throwable {
		Producto producto = productoService.getOne(id);
		ProductoForm form = ProductoFormAProductoMapper.productoAProductoForm(producto);
		return form;
	}

	@GetMapping("/productos/xml")
	@ResponseBody
	public List<ProductoForm> exportarProductos() throws Throwable {
		return productoService.getAll().stream().map(p -> ProductoFormAProductoMapper.productoAProductoForm(p))
				.collect(Collectors.toList());
	}
	
	@GetMapping("/producto/{id}/borrarImagen")
	public ModelAndView borrarImagen(@PathVariable("id") Integer id) throws Throwable {
		Producto producto = productoService.getOne(id);
		producto.setImagen(null);
		producto = productoService.save(producto);
		return new ModelAndView("redirect:/");
	}
	
	
}