package com.example.prueba.controllers;

import com.example.prueba.exceptions.ProductoNotFoundException;
import com.example.prueba.mappers.ProductoFormAProductoMapper;
import com.example.prueba.models.Categoria;
import com.example.prueba.models.Producto;
import com.example.prueba.models.ProductoForm;
import com.example.prueba.services.CategoriaService;
import com.example.prueba.services.ProductoService;


import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * Controlador web.
 * 
 * @author obarcia
 */
@RestController
@RequestMapping("/productos")
public class ProductoRestController
{
    /**
     * Servicio de productos.
     */
    @Autowired
    private ProductoService productoService;
    
    /**
     */
    @GetMapping
    public List<Producto> getProductos()
    {
        return productoService.getAll();
    }
    /**
     * Página de un producto.
     * @param id Identificador del producto.
     * @throws ProductoNotFoundException 
     * @throws java.lang.Throwable
     */
    @GetMapping(value = "/{idProducto}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ProductoForm getProducto(@PathVariable int idProducto) throws ProductoNotFoundException{
		Producto producto;
		ProductoForm form = null;

		producto = productoService.getOne(idProducto);
		form = ProductoFormAProductoMapper.productoAProductoForm(producto);
	
		return form;
	}
    
    @GetMapping(value = "/categoria/{idCategoria}", produces = MediaType.APPLICATION_JSON_VALUE)
 	public List<ProductoForm> getProductosPorCategoria(@PathVariable int idCategoria) throws ProductoNotFoundException{
 		
    	return productoService.getProductoByCategoriaId(idCategoria);
 	
 	}
    
}