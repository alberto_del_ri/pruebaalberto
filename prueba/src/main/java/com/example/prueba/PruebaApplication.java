package com.example.prueba;

import java.util.TimeZone;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class PruebaApplication extends SpringBootServletInitializer
{
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application)
    {
        TimeZone.setDefault(TimeZone.getTimeZone("Europe/Madrid"));
        
        return application.sources(PruebaApplication.class);
    }
    public static void main(String[] args)
    {
        new SpringApplicationBuilder(PruebaApplication.class)
                .build()
                .run(args);
    }
}
