package com.example.prueba.exceptions;

/**
 * Excepción de categoria no encontrada.
 * 
 * @author mtellechea
 */
public class CategoriaNotFoundException extends Exception {}