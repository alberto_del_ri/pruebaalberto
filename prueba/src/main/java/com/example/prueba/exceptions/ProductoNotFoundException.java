package com.example.prueba.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

/**
 * Excepción de producto no encontrado.
 * 
 * @author obarcia
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ProductoNotFoundException extends Exception {

	
}