package com.example.prueba.models;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;


/**
 * Formulario de un producto.
 * 
 * @author obarcia
 */
public class CategoriaForm
{
    
    /**
     * Nombre.
     */
    @NotEmpty
    @Size(max = 160)
    private String nombre = "";
    /**
     * Descripción.
     */
    @Size(max = 258)
    private String descripcion = "";
    
    // **********************************************
    // GETTER & SETTER
    // **********************************************
    public String getNombre()
    {
        return nombre;
    }
    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }
    public String getDescripcion()
    {
        return descripcion;
    }
    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }
}