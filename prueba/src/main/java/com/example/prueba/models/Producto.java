package com.example.prueba.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Modelo de un producto.
 * 
 * @author obarcia
 */
@Entity
@Table(name = "productos_00")
@JacksonXmlRootElement(localName = "PRODUCTO")
public class Producto implements Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
     * Identificador.
     */
    @Id
    @GeneratedValue
    @Column(name = "id")
    @JsonProperty("ID")
    private Integer id;
    /**
     * Nombre.
     */
    @Column(name = "name")
    @JsonProperty("NOMBRE")
    private String nombre;
    /**
     * Descripción.
     */
    @Column(name = "description")
    @JsonProperty("DESCRIPCION")
    @XmlTransient
    private String descripcion;
    /**
     * Datos de la imagen.
     */
    @Column(name = "image", columnDefinition="BLOB")
    @JsonProperty("IMAGEN")
   
    private byte[] imagen;
    
    @ManyToOne
    @JoinColumn(name="id_categoria")
    @JsonProperty("CATEGORIA")
    private Categoria categoria; 
    
    // **********************************************
    // GETTER & SETTER
    // **********************************************
    public Integer getId()
    {
        return id;
    }
    public void setId(Integer id)
    {
        this.id = id;
    }
    public String getNombre()
    {
        return nombre;
    }
  
    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }
    public String getDescripcion()
    {
        return descripcion;
    }
    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }
    @XmlTransient
    public byte[] getImagen()
    {
        return imagen;
    }
    @XmlTransient
    public void setImagen(byte[] imagen)
    {
        this.imagen = imagen;
    }
	public Categoria getCategoria() {
		return categoria;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
    
    
}