package com.example.prueba.models;

import java.io.InputStream;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * Formulario de un producto.
 * 
 * @author obarcia
 */
@XmlRootElement(name = "producto")

public class ProductoForm
{
    /**
     * Nombre.
     */
    @Size(max = 160,message = "no puede tener mas de 160 caracteres")
    @NotNull
    private String nombre = "";
    /**
     * Descripción.
     */
    @Size(max = 258,message = "no puede tener mas de 258 caracteres")
    @NotNull
    private String descripcion = "";
    /**
     * Imagen
     */
    @Size(max = 1024,message = "no puede tener mas de 258 caracteres")
    InputStream imagen; 
    
    /**
     * Descripción.
     */
    @Size(max = 258)
    private String categoria = "";
    
    // **********************************************
    // GETTER & SETTER
    // **********************************************
    public String getNombre()
    {
        return nombre;
    }
    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }
    public String getDescripcion()
    {
        return descripcion;
    }
    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public InputStream getImagen() {
		return imagen;
	}
	public void setImagen(InputStream imagen) {
		this.imagen = imagen;
	}
	
	
    
}