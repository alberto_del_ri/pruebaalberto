package com.example.prueba.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Categoria de un producto.
 * 
 * @author mtellechea
 */
@Entity
@Table(name = "categorias_00")
@JacksonXmlRootElement(localName = "CATEGORIA")
public class Categoria implements Serializable
{
    /**
     * Identificador.
     */
    @Id
    @GeneratedValue
    @Column(name = "id")
    @JsonProperty("ID")
    private Integer id;
    /**
     * Nombre.
     */
    @Column(name = "name")
    @JsonProperty("NOMBRE")
    private String nombre;
    /**
     * Descripción.
     */
    @Column(name = "description")
    @JsonProperty("DESCRIPCION")
    private String descripcion;
   
    // **********************************************
    // GETTER & SETTER
    // **********************************************
    public Integer getId()
    {
        return id;
    }
    public void setId(Integer id)
    {
        this.id = id;
    }
    public String getNombre()
    {
        return nombre;
    }
    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }
    public String getDescripcion()
    {
        return descripcion;
    }
    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }
}