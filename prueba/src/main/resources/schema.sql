CREATE TABLE productos_00 (
   id INT AUTO_INCREMENT  PRIMARY KEY,
   name VARCHAR(250) NOT NULL,
   description VARCHAR(255) NOT NULL,
   image BINARY
);

INSERT INTO `productos_00` (`id`, `name`, `description`,`image`) VALUES
(1, 'Producto 01', 'producto 01','7e');