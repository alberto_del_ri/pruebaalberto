<%@tag description="Main Layout" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="ui" uri="http://www.gestores.net/tags" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@attribute name="title" required="false" type="java.lang.String" %>
<%@attribute name="lang" required="false" type="java.lang.String" %>
<c:set var="lang" value="${(empty lang) ? 'es' : lang}" />
<c:set var="title" value="${(empty title) ? 'Prueba' : title}" />
<!DOCTYPE html>
<html lang="${lang}">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><c:out value="${title}" /></title>
        <link href="https://fonts.googleapis.com/css?family=Roboto:700,700italic,500italic,500,400,400italic,300|Oxygen:400,700|Roboto+Condensed:400,700" rel="stylesheet" type="text/css">
        <link href="<c:url value="/resources/bootstrap/v4/css/bootstrap.min.css" />" rel="stylesheet" type="text/css">
        <link href="<c:url value="/resources/fontawesome/all.css" />" rel="stylesheet" type="text/css">
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        
        <script src="<c:url value="/resources/jquery/jquery.min.js" />"></script>
        <script src="<c:url value="/resources/bootstrap/v4/js/popper.min.js" />"></script>
        <script src="<c:url value="/resources/bootstrap/v4/js/bootstrap.min.js" />"></script>
    </head>
    <body>
        <div class="container">
            <div class="wrap">
                <div class="wrap-container" style="padding-top:64px;">
                    <jsp:doBody />
                </div>
            </div>
        </div>
    </body>
</html>
