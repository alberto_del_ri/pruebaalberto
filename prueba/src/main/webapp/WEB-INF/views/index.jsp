<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="ui" uri="http://www.gestores.net/tags" %>
<ui:layout title="${title}">
    <div class="form-group">
        <a class="btn btn-success" href="<c:url value="/producto/0" />">Nuevo producto</a>
        <a class="btn btn-success" href="<c:url value="/categoria" />">Ver Categorias</a>
        <a class="btn btn-primary" target="_blank" href="<c:url value="/productos/xml" />">Exportar a XML</a>
    </div>
    <table class="table table-stripped">
        <thead>
            <tr>
                <th style="width:10%;">ID</th>
                <th style="width:30%;">Nombre</th>
                <th style="width:50%;">Descripci�n</th>
                <th style="width:50%;">Categoria</th>
                <th style="width:10%;">Imagen</th>
                <th style="width:1px;"></th>
                <th style="width:1px;"></th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${list}" var="p">
                <tr>
                    <td><c:out value="${p.id}" /></td>
                    <td><c:out value="${p.nombre}" /></td>
                    <td><c:out value="${p.descripcion}" /></td>
                    <td><c:out value="${p.categoria.nombre}" /></td>
                    <td><img style="width:200px;" src="<c:url value="/producto/${p.id}/image" />" /></td>
                    <td><a class="btn btn-primary" href="<c:url value="/producto/${p.id}" />"><i class="fa fa-edit" /></a></td>
                    <td><a class="btn btn-primary" href="<c:url value="/producto/${p.id}/exportar" />"><i class="fa fa-file-code-o" /></a></td>
                    <td><a class="btn btn-danger" onclick="return confirm('�Esta seguro que desea eliminar el producto?')"  href="<c:url value="/producto/${p.id}/delete" />"><i class="fa fa-times" /></a></td>
                	<td><a class="btn btn-danger"  href="<c:url value="/producto/${p.id}/borrarImagen" />"><i class="fa fa-image" /></a></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</ui:layout>