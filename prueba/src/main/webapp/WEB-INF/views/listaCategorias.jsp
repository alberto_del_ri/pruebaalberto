<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="ui" uri="http://www.gestores.net/tags" %>
<ui:layout title="${title}">
    <div class="form-group">
       <a class="btn btn-success" href="<c:url value="/" />">HOME</a>
        <a class="btn btn-success" href="<c:url value="/categoria/0" />">Nueva Categoria</a>
      
    </div>
    
    <table class="table table-stripped">
        <thead>
            <tr>
                <th style="width:10%;">ID</th>
                <th style="width:30%;">Nombre</th>
                <th style="width:50%;">Descripci�n</th>
                <th style="width:1px;"></th>
                <th style="width:1px;"></th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${list}" var="p">
                <tr>
                    <td><c:out value="${p.id}" /></td>
                    <td><c:out value="${p.nombre}" /></td>
                    <td><c:out value="${p.descripcion}" /></td>
                    <td><a class="btn btn-primary" href="<c:url value="/categoria/${p.id}" />"><i class="fa fa-edit" /></a></td>
                    <td><a class="btn btn-danger" onclick="return confirm('�Esta seguro que desea eliminar la categoria?')"  href="<c:url value="/categoria/${p.id}/delete" />"><i class="fa fa-times" /></a></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</ui:layout>