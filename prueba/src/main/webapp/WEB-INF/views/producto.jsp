<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="ui" uri="http://www.gestores.net/tags"%>
<ui:layout title="${title}">
	<c:if test="${not empty flash}">
		<div class="alert alert-success">
			<c:out value="${flash}" />
		</div>
	</c:if>

	<form:form modelAttribute="form" method="POST" acceptCharset="UTF-8"
		enctype="multipart/form-data" autocomplete="off">
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					<form:label path="nombre">Nombre</form:label>
					<form:input class="form-control" path="nombre"/>
					<span class="help-block help-error"><form:errors
							path="nombre" /></span>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					<form:label path="descripcion">Descripci�n</form:label>
					<form:textarea class="form-control" path="descripcion" />
					<span class="help-block help-error"><form:errors
							path="descripcion" /></span>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
				<select name="categoria" path="categoria">
					<c:forEach var="categoria" items="${categorias}">
						<option value="${categoria.nombre}">${categoria.nombre}</option>
					</c:forEach>
				</select>
				</div>
			</div>
		</div>
				<br>
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					<label>Imagen</label>
					<td> - Formatos JPG o PNG soportados <input type="file" name="file" /> </td>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					<form:button class="btn btn-primary">GUARDAR</form:button>
					&nbsp; <a class="btn btn-success" href="<c:url value="/" />">ATR�S</a>
				</div>
			</div>
		</div>
	</form:form>
</ui:layout>