-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-09-2019 a las 08:18:32
-- Versión del servidor: 5.7.14
-- Versión de PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd_productos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_00`
--

CREATE TABLE `productos_00` (
  `id` int(6) UNSIGNED NOT NULL,
  `name` varchar(160) DEFAULT NULL,
  `description` varchar(258) DEFAULT NULL,
  `image` longblob,
  `id_categoria` int(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `categorias_00` (
  `id` int(6) UNSIGNED NOT NULL,
  `name` varchar(160) DEFAULT NULL,
  `description` varchar(258) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `productos_00`
--
ALTER TABLE `productos_00`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `productos_00`
--
ALTER TABLE `productos_00`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT;
  
ALTER TABLE productos_00 ADD CONSTRAINT fk_categoria FOREIGN KEY (id_categoria) 
REFERENCES categoria (id);

ALTER TABLE `categorias_00`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `categorias_00`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT;
  
  select *
  from categorias_00;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
